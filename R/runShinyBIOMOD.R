#' Running ShinyBIOMOD
#'
#' Runs shinyBIOMOD application
#'
#' @return None
#' @export
#' @examples
#' run_shinyBIOMOD()
run_shinyBIOMOD <-function(){

	if(appdir() == ""){
		stop("Couldn't find application directory.Try re-installing ShinyBIOMOD", call.=FALSE)
	}
	shiny::runApp(appdir(), display.mode="auto")
}

#' Returns the path to the top directory of ShinyBIOMOD
#' @export
appdir <- function() system.file("ShinyApp", package="ShinyBIOMOD")
