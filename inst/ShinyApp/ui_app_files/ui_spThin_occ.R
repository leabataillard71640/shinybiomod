spthin_occ <- fluidPage(

							useShinyjs(),

							extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

							titlePanel(h1(strong(em("Spatial Thinning of Species Occurrence Data")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

							sidebarLayout(position = "left",

								sidebarPanel(

									tags$head(

										#tags$style(HTML('#save_spThin_data{color:black; background-color:white; border-color: black}')),

										tags$script(src = 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript')

									),

									hr(),

									div(style="text-align: justify;",
										p("Thin your species occurrence points according to a specified distance using the ",a(href="https://cran.r-project.org/web/packages/spThin/spThin.pdf",strong(em("spThin"))),"package."),
										tags$ul(style="font-family: 'times';font-size: 18px;",
										        tags$li("Set a",strong(em("thinning distance")),"i.e. a minimum distance separating two occurrence records."),
										        tags$li("This will help to", strong(em("reduce the effect of sampling bias and autocorrelation")),"within the collection of records.")
										)
									),

									hr(),

									h3("Settings"),

									p(strong("Select your occurence dataset")),

									selectizeInput(inputId="spThin_data_from_occ_dataset",

										label = NULL,

										choices = NULL,

										options = list(placeholder = "Select occurence data")
									),

									hidden(
										div(id='spThin_data_with_selected_species',

											p(strong('Select your species')),

											selectizeInput(inputId='spThin_data_species',

												label=NULL,

												choices = NULL,

												options=list(placeholder="Select a species")
											)
										)
									),
									#uiOutput('spThin_data_with_selected_species'),

									uiOutput("spThin_analysis")

									#uiOutput('spThin_diagnosis')

								),

								mainPanel(

									#tabsetPanel(

										tabPanel("Map",

											useShinyjs(),

											withSpinner(leafletOutput(outputId="SpThinMap", width = "100%", height="700px"),type=5,size=1, color='#000000'),

											hidden(
												tags$style(appCSS),
												div(id="save_button_spThin_data", align="left",
												    uiOutput("spthin_summary_text_output")
												)
											)
										)#,
										# tabPanel("Summary",
										# 	uiOutput("spThin_analysis_outputs")
										# )
									#)

								)
							)
						)
