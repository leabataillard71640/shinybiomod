best_subsets <- fluidPage(

							useShinyjs(),

							extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),


							titlePanel(h1(strong(em("Best Subsets of Variables")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

							sidebarLayout(position = "left",

								sidebarPanel(

									tags$head(

									  tags$script(src = 'https://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript')

									),

									hr(),

									p(style="text-align: justify","Select the",em(strong("best subset")),"of variables using the",a(href="https://cran.r-project.org/web/packages/bestglm/bestglm.pdf",strong(em("bestglm"))),em("package.")),
									tags$ul(
									tags$li(p("Compare the results of different", em(strong("information criteria"),em("(e.g. AIC, BIC)")),"or", em(strong("cross-validation methods"),em("(e.g. K-fold).")))),
									tags$li(p("Several",em(strong("search methods")),"are also available",em("(e.g. exhaustive search, forward or backward stepwise).")))
									),
									hr(),

									strong(p("Select your Presence/Absence dataset")),

									selectizeInput(inputId="best_subsets_data_from_pa_dataset",

										label = NULL,

										choices = NULL,

										options = list(placeholder = "Select P/A data")
									),

									uiOutput('best_subsets_data_with_selected_species'),

									strong(p("Select environmental data")),

									selectizeInput(inputId="best_subsets_data_from_env_layers",

										label = NULL,

										choices = NULL,

										multiple = TRUE,

										options = list(placeholder="Select environmental layers dataset(s)")
									),

									p(strong("Select p variables"),em(strong("(p",icon('less-than-equal','fa-0.8x', lib="font-awesome"),"15)"))),

									selectizeInput(inputId="best_subsets_data_from_explvar",

										label = NULL,

										choices = NULL,

										multiple = TRUE,

										options = list(placeholder="Select explanatory variables", maxItems = 15)
									),

									hr(),

									div(id="best_subsets_analysis_panel",

									    radioButtons(inputId='best_subsets_information_criteria',

									                 label = strong('Select a criteria to use:'),

									                 choices = c('AIC'='AIC','BIC'='BIC', 'BICg'='BICg','BICq'='BICq','CV'='CV'),

									                 selected = 'BIC',

									                 inline = TRUE
									    )

									),

									hidden(
									  div(id="best_subsets_analysis_panel_part2",
									    uiOutput("best_subsets_analysis"),
									    hr()
									  )
									)

								),

								mainPanel(

									uiOutput("best_subsets_analysis_outputs")

								)
							)
						)
