ui_bioprojection <- sidebarLayout(position = "left",

					useShinyjs(),

					extendShinyjs("www/js/app-shinyjs.js", functions = c("getInputType")),

					sidebarPanel(

						tags$head(

							tags$script(src = 'http://cdn.mathjax.org/mathjax/latest/MathJax.js?config=TeX-AMS-MML_HTMLorMML', type = 'text/javascript',includeScript("style/google-analytics.js"))

						),

						titlePanel(h1(strong(em("BioMod 2")), style = "font-family: 'times', cursive; font-weight: 500; line-height: 1.1;")),

						p("Here you can process",strong("Species Distribution Modeling"),"within an Ensemble Forecasting framework using the",

						a(href="https://cran.r-project.org/web/packages/biomod2/biomod2.pdf",target='_blank',"biomod2"),"package"),

						br(),

						h3("Projecting models"),

						hr(),

						p("Select the species whose you want to model the distribution"),

						uiOutput("chooserProjectingSpecies"),

						br(),

						h4("Settings"),

						wellPanel(

              p("Select the new geographic space where you want to make forecasting"),

							selectizeInput(inputId="biomod_projecting_new_area",

								label = NULL,

								choices = NULL,

								options=list(placeholder="Select area extent")

							),

							p("Select new environmental variables"),

							selectizeInput(inputId='biomod_projecting_new_env',

								label = NULL,

								choices = NULL,#c("Your own raster layers" = "user_layers", "WorldClim current raster layers" = "wc_current_layers", "WorldClim futur raster layers" = "wc_futur_layers"),

								multiple = TRUE

								#selected ="wc_futur_layers"

							),

							br(),

							uiOutput("biomod_projecting_new_env_ui"),

							br(),

							h5("Selected models"),

							selectInput(inputId='biomod_projecting_selected_models',

								label = 'Select one (or more) models',

								choices = c('ALL'='all','GLM'='GLM','GBM'='GBM', 'GAM'='GAM','CTA'= 'CTA','ANN'= 'ANN', 'SRE'='SRE', 'FDA'='FDA', 'MARS'='MARS','RF'= 'RF', 'MAXENT'='MAXENT'),

								multiple=TRUE,

								selected= 'all'
							),

							h5("Binary method"),

							selectInput(inputId='biomod_projecting_BinaryMeth',

								label = 'Select one (or more) evaluation metric(s)',

								choices = c('KAPPA'='KAPPA', 'TSS'='TSS', 'ROC'='ROC', 'FAR'='FAR', 'SR'='SR', 'ACCURACY'='ACCURACY', 'BIAS'='BIAS', 'POD'='POD', 'CSI'='CSI' ,'ETS'='ETS'),

								multiple=TRUE,
							),

							h5("Filtered method"),

							selectInput(inputId='biomod_projecting_FilteredMeth',

								label = 'Select one (or more) evaluation metric(s)',

								choices = c('KAPPA'='KAPPA', 'TSS'='TSS', 'ROC'='ROC', 'FAR'='FAR', 'SR'='SR', 'ACCURACY'='ACCURACY', 'BIAS'='BIAS', 'POD'='POD', 'CSI'='CSI' ,'ETS'='ETS'),

								multiple=TRUE,
							),

							h5("Compression format"),

							radioButtons(inputId='biomod_projecting_compress_format',label='select a compression format',choices=c('TRUE'='TRUE','FALSE'='FALSE','XZ'='xz','GZIP'='gzip'), selected='xz',inline=TRUE)
						),

						tags$style(appCSS),

						useShinyjs(),

						p("Run your projection"),

						withBusyIndicatorUI(

							actionButton(inputId="biomod_run_projecting",

								label = "Run BIOMOD Projection",

								style = "primary"
							)
						),
						busyIndicator("Projection in progress...",wait=2000)
					),

					mainPanel(


					)

				)
