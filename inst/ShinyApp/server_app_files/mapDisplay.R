

slider_opacity_user_occ_map <- shiny::debounce(reactive(input$map_circle_opacity), millis=1000) # delay slider input reactivity

# --------------------------------------------------------

# OBSERVERS

# --------------------------------------------------------
observe({

	if(!is.null(list_species_toMap()) && nchar(list_species_toMap())>0L){
		shinyjs::show("controls_species")
	}else{
		shinyjs::hide("controls_species")
	}

	if(!is.null(input$Select_occ_data_by_attributes) && input$Select_occ_data_by_attributes =='YES' && !is.null(list_attribute_toMap()) && nchar(list_attribute_toMap())>0L){
		shinyjs::show("controls_attributes")
	}else{
		shinyjs::hide("controls_attributes")
	}

})

# shows/hides add polygon region checkbox
observe({

	if(!is.null(myRegion())){
		shinyjs::show("add_myRegion_checkbox")
	}else{
		shinyjs::hide("add_myRegion_checkbox")
	}
})


# updates show/hide of the map occurrence attribute chooser
# observe({

	# if(!is.null(input$Select_occ_data_by_attributes) && input$Select_occ_data_by_attributes == 'NO'){
		# if(is.null(input$User_Group_name) || is.null(input$map_occ_dataset) || !input$map_occ_dataset %in% c("raw_occ","filter_occ","clean_occ"))
			# shinyjs::hide("map_occ_attributes_chooser")
	# }
	# else{
		# if(!is.null(input$User_Group_name) && !is.null(input$map_occ_dataset) && input$map_occ_dataset %in% c("raw_occ","filter_occ","clean_occ"))
			# shinyjs::show("map_occ_attributes_chooser")
	# }
# })


# Observer to update list of available points dataset
observe({

	map_data <- NULL

	if(!is.null(data_input()))
		map_data <- c(map_data, "Your raw occurence data" = "raw_occ")

	if(!is.null(data_select()))
		map_data <- c(map_data, "Your subsetted occurence data" = "subset_occ")

	if(!is.null(data_clean()))
		map_data <- c(map_data, "Your cleaned occurence data" = "clean_occ")

	#selected_occ_data <- if("filter_occ"%in%input$map_occ_dataset)  "filter_occ" else NULL

	updateSelectizeInput(session, "map_occ_dataset", choices=map_data, selected=input$map_occ_dataset)

})


# colorPal <- reactive({

	# if(!is.null(input$map_species_colors)){
		# #pal = brewer.pal(n=length(input$map_occ_species_name$right), name=input$map_species_colors)
		# return(colorFactor(input$map_species_colors, filtered_data()[,input$User_Species_name]))
	# }
	# else
		# return(NULL)
# })

# Observer to update polygon parameters
observe({

	if(!is.null(filtered_data()) && !is.null(myRegion()) && !is.null(input$add_myRegion) && input$add_myRegion){

		pal <- colorPal()

		proxy <-leaflet::leafletProxy("myOccMap", data = filtered_data())

		proxy <- proxy %>% leaflet::clearShapes()

		proxy <- proxy %>%

		  leaflet::addPolygons(data = myRegion(),

		            weight = 3,

			          fillColor = "grey",

			          color = "black",

			          stroke=TRUE,

			          fillOpacity =  min(slider_opacity_user_occ_map(),0.5)
		)

		proxy <- proxy %>%

		  leaflet::addCircles(radius = input$map_circle_size,

			lng = filtered_data()[,data_set()$longitude],

			lat = filtered_data()[,data_set()$latitude],

			popup = filtered_data()$ID_shbmd,

			color = pal(filtered_data()[,input$User_Species_name]),#"#777777",

			#fillColor = ~,

			fillOpacity = slider_opacity_user_occ_map()
		)

	}
})

# Observer to update circles when a new color is chosen
observe({

	pal <- colorPal()

	if(!is.null(filtered_data())){

		proxy <-leaflet::leafletProxy("myOccMap", data = filtered_data())

		proxy <- proxy %>% leaflet::clearShapes()

	  if(!is.null(myRegion()) && !is.null(input$add_myRegion) && input$add_myRegion){

	    proxy <- proxy %>%

	      leaflet::addPolygons(data = myRegion(),

	                  weight = 3,

	                  fillColor = "grey",

	                  color = "black",

	                  stroke=TRUE,

	                  fillOpacity = min(slider_opacity_user_occ_map(),0.5)
	      )
	  }

		proxy <- proxy %>%

		  leaflet::addCircles(radius = input$map_circle_size, data = filtered_data(),

			lng = filtered_data()[,data_set()$longitude],

			lat = filtered_data()[,data_set()$latitude],

			popup = filtered_data()$ID_shbmd,

			color = pal(filtered_data()[,input$User_Species_name]),#"#777777",

			#fillColor = ~,

			fillOpacity = slider_opacity_user_occ_map()
		)

	}
})

# observer to update the legend of 'map'
observe({

	if(!is.null(filtered_data())){

		proxy <- leaflet::leafletProxy("myOccMap", data = filtered_data())

		# Remove any existing legend, and only if the legend is enabled, create a new one.
		proxy <- proxy %>% leaflet::clearControls()

		if(input$add_Legend){

			pal <- colorPal()

			proxy <- proxy %>%

			  leaflet::addLegend(position = "topleft",

				title= "Species",

				pal = pal,

				values = filtered_data()[,input$User_Species_name],

				labels = gsub("[_|\\.]"," ",input$map_occ_species_name$right)
			)
		}
	}
})


# Observer to update input$map_species_colors!='Select a color scheme'
observeEvent(input$map_species_colors,{
	# Update select input for palette name
	if(input$map_species_colors!='Select a color scheme')
		updateSelectizeInput(session, 'map_species_colors', selected=input$map_species_colors)
})

# --------------------------------------------------------

# REACTIVE FUNCTIONS

# --------------------------------------------------------

leafMap <- reactive({

  # Draw map leaflet map

  map <- leaflet::leaflet(options = leaflet::leafletOptions(worldCopyJump=T)) %>%

    leaflet::addProviderTiles(leaflet::providers$Esri.OceanBasemap, group="Esri")

  # Add polygon
  if(!is.null(myRegion()) && !is.null(input$add_myRegion) && input$add_myRegion){

    map <- map %>%

      leaflet::addPolygons(data = myRegion(),

                  weight = 3,

                  fillColor = "grey",

                  color = "black",

                  stroke=TRUE,

                  fillOpacity = min(slider_opacity_user_occ_map(),0.5)
      )
  }

  # Set extent
  if(!is.null(filtered_data())){

    map <- map %>%

      setView(lat=median(filtered_data()[,data_set()$latitude]),lng=median(filtered_data()[,data_set()$longitude]), zoom=4, options=list(maxZoom=11))
  }
  else{
    map <- map %>%

      setView(lat=0,lng=0, zoom=3, options=list(maxZoom=11))
  }
  return(map)

})

# Return occurence dataset selected by user
data_set <- reactive({

  data <- NULL

	if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "raw_occ"){

		data <- data_input()
		if(is.null(data)) return(data)

		data$ID_shbmd <- 1:nrow(data)

		return(list(data=data,

					longitude=input$User_XLongitude,

					latitude= input$User_YLatitude)
		)
	}

	if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "subset_occ"){

		data <- data_select()
		if(is.null(data)) return(data)

		data$ID_shbmd <- 1:nrow(data)

		return(list(data=data,

					longitude=input$User_XLongitude,

					latitude= input$User_YLatitude)
		)

	}

	if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "clean_occ"){

		data <- data_clean()
		if(is.null(data)) return(data)

		data$ID_shbmd <- 1:nrow(data)

		return(list(data=data,

					longitude=input$User_XLongitude,

					latitude= input$User_YLatitude)
		)
	}

	return(data)

})

# Return dataset filtered by user species choices and/or locations
filtered_data <- reactive({

	data <- data_set()$data
	input$map_occ_species_name$right
	input$map_occ_attributes_name$right

	isolate({
		data_filter <- NULL

		if(!is.null(data) && length(input$map_occ_species_name$right)>0 && length(input$map_occ_attributes_name$right)==0)
			data_filter <- data[c(data[,input$User_Species_name] %in% input$map_occ_species_name$right),]

		else if(!is.null(data) && length(input$map_occ_attributes_name$right)>0 && length(input$map_occ_species_name$right)==0)
			data_filter <- data[c(data[,input$User_Group_name] %in% input$map_occ_attributes_name$right),]

		else if(!is.null(data) && length(input$map_occ_species_name$right)>0 && length(input$map_occ_attributes_name$right)>0)
			data_filter <- data[c(data[,input$User_Species_name] %in% input$map_occ_species_name$right) & c(data[,input$User_Group_name] %in% input$map_occ_attributes_name$right), ]

	})
	return(data_filter)
})

# list of species names available for plotting
list_species_toMap <- reactive({

	species_names = NULL

	# Data frame with occurences records
	if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "raw_occ")
		data <- data_input()
	else if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "subset_occ")
		data <- data_select()
	else if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "clean_occ")
		data <- data_clean()

	isolate({
		if(!is.null(input$User_Species_name) && input$User_Species_name!="Upload your data" && input$User_Species_name %in% names(data)){
			species_names = eval(parse(text=sprintf("levels(factor(unique(data$%s)))",input$User_Species_name)))
		}
	})

	return(species_names)
})

# list of locations names
list_attribute_toMap <- reactive({

	attribute_names = NULL

	# Data frame with occurences records
	if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "raw_occ")
		data <- data_input()
	else if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "subset_occ")
		data <- data_select()
	else if(!is.null(input$map_occ_dataset) && input$map_occ_dataset == "clean_occ")
		data <- data_clean()

	isolate({
		if(!is.null(input$User_Group_name) && input$User_Group_name!="Upload your data" && input$User_Group_name %in% names(data)){
			attribute_names = eval(parse(text=sprintf("levels(factor(unique(data$%s)))",input$User_Group_name)))
		}
	})
	return(attribute_names)
})

# Set colours
colorPal <-reactive({

  if(!is.null(filtered_data()) && !is.null(input$map_species_colors)){

    spCol<-filtered_data()

    suppressWarnings(
      colRamp <- colorRampPalette(brewer.pal(n=length(levels(spCol[,input$User_Species_name])), name=input$map_species_colors))
    )
    cols <- colRamp(length(levels(as.factor(spCol[,input$User_Species_name]))))

    factpal <- colorFactor(cols, spCol[,input$User_Species_name])

    return(factpal)
  }
  else
    return(NULL)
})

# --------------------------------------------------------

# OUTPUTS

# --------------------------------------------------------
output$myOccMap <- renderLeaflet({

  return(leafMap())

})


output$chooser_map_occ_species_name <- renderUI({

  chooserInput(inputId="map_occ_species_name",

               leftLabel="Available species",

               rightLabel="Selected species",

               leftChoices= list_species_toMap(),

               rightChoices=c(),

               size = min(6,length(list_species_toMap())),

               multiple=TRUE
  )

})

output$chooser_map_occ_attributes_name <- renderUI({

  chooserInput(inputId="map_occ_attributes_name",

               leftLabel="Available attributes",

               rightLabel="Selected attributes",

               leftChoices= list_attribute_toMap(),

               rightChoices=c(),

               size = min(6,length(list_attribute_toMap())),

               multiple=TRUE
  )
})








