# Name: arcgisMapping.py
# Description: Create ArcMap template document 
# Requirements: ArcGIS Mapping Extension

# Import system modules
import os
import arcpy
import re
from arcpy import env
from arcpy.mapping import *

# Get command line arguments
import sys
args = sys.argv

# Define current working space 
workspace = args[1].split('=')
env.workspace = workspace[1]

# Allow overwriting
env.overwriteOutput = True

# Set local variable
_template_	= args[2].split('=')
template	= _template_[1]
_outfile_ 	= args[3].split('=')
outfile		= _outfile_[1]

# Check out ArcGIS Mapping extension license
arcpy.CheckOutExtension('Mapping')

# Read the original template 
mxd = MapDocument(template)

# Remove it if file already exists
if arcpy.Exists(outfile):
	# Delete the copy
	arcpy.Delete_management(outfile)
		
# Make a copy of the template
mxd.saveACopy(outfile)

# Read the template copy
mxd = MapDocument(outfile)

######################
# Updates to process #
######################
change_title = False
change_desc = False
change_layers = False
change_extent = False
change_picture = False
change_inset_title = False

##################################
# Check if updates are available #
##################################
for arg in args[4:]:
	arg = arg.split('=')
	if arg[0] == 'title_name_page_elt':
		title_name_page_elt = arg[1]
		change_title = True
	if arg[0] == 'desc_name_page_elt':
		desc_name_page_elt = arg[1]
		change_desc = True
	if arg[0] == 'inset_title':
		inset_title = re.sub('_',' ',arg[1])
		change_inset_title = True
	if arg[0] == 'data_species' or arg[0] == 'raster_layer':
		change_layers = True
	if arg[0] == 'thumbnail_picture':
		change_picture = True
	if arg[0] == 'extent_map' or 'xmin' or arg[0] == 'xmax' or arg[0] == 'ymin' or arg[0] == 'ymax':
		change_extent = True
		
####################
# Change the title #
####################
if change_title == True:
	title_elm = ListLayoutElements(mxd, "TEXT_ELEMENT", title_name_page_elt)[0]
	for arg in args[4:]:
		arg = arg.split('=')
		if arg[0] == 'title':
			title_elm.text = re.sub('_',' ',arg[1])

##########################
# Change the description #
##########################
if change_desc == True:
	desc_elm = ListLayoutElements(mxd, "TEXT_ELEMENT", desc_name_page_elt)[0]
	old_text = desc_elm.text
	new_desc = old_text
	for arg in args[4:]:
		arg = arg.split('=')
		if arg[0] == 'species_name':
			sp_name = re.sub('_',' ',arg[1])
			new_desc = re.sub('_SP_',sp_name,new_desc)		
		if arg[0] == 'total_num_models':
			new_desc = re.sub('_TNM_',arg[1],new_desc)
		if arg[0] == 'num_models':
			new_desc = re.sub('_NM_',arg[1],new_desc)
		if arg[0] == 'region':
			region = re.sub('_',' ',arg[1])
			new_desc = re.sub('_REG_', region, new_desc)
		if arg[0] == 'models_names':
			new_desc = re.sub('_MN_',re.sub('_',' ',arg[1]),new_desc)	
		if arg[0] == 'eval_metrics':
			new_desc = re.sub('_EvMet_',arg[1],new_desc)	
		if arg[0] == 'data_splitting':
			new_desc = re.sub('_DS_',arg[1],new_desc)
		if arg[0] == 'num_pa_dat':
			new_desc = re.sub('_NPAdat_',arg[1],new_desc)			
		if arg[0] == 'threshold_rules':
			threshold_rule = re.sub('_',' ',arg[1])
			new_desc = re.sub('_TR_', threshold_rule, new_desc)
		if arg[0] == 'num_kept_models':
			new_desc = re.sub('_NKM_',arg[1],new_desc)	
		if arg[0] == 'eval_metric_score':
			eval_metrics_scores = re.sub('_',' ',arg[1])
			new_desc = re.sub('_EvS_', eval_metrics_scores, new_desc)
		if arg[0] == 'run':
			new_desc = re.sub('_RUN_',arg[1],new_desc)	
		if arg[0] == 'num_pres':
			new_desc = re.sub('_NP_',arg[1],new_desc)	
		if arg[0] == 'num_pa':
			new_desc = re.sub('_NPA_',arg[1],new_desc)	
		if arg[0] == 'var_imp':
			new_desc = re.sub('_VI_',re.sub('_',' ',arg[1]),new_desc)
			
	desc_elm.text = new_desc
	
##########################
# Change the inset title #
##########################
if change_inset_title == True:
	for elm in ListLayoutElements(mxd, "TEXT_ELEMENT"):
		if(elm.name=='Inset title'):
			elm.text = inset_title
			
######################
# Change the layers  #
######################
if change_layers == True:
	for lyr in	ListLayers(mxd):
		# update species occurence shapefile source layer
		if lyr.isFeatureLayer == True:
			if lyr.name == 'DataSpecies':
				for arg in args[4:]:
					arg = arg.split('=')
					if arg[0] == 'data_species':
						if lyr.name == re.sub('.shp','',arg[1]):
							lyr.replaceDataSource(env.workspace,'SHAPEFILE_WORKSPACE',re.sub('.shp','',arg[1]))
							lyr.name = re.sub('.shp','',arg[1])			
		# update species distribution raster source layer	
		if lyr.isRasterLayer == True:
			for arg in args[4:]:
				arg = arg.split('=')
				if arg[0] == 'raster_layer':
					lyr.replaceDataSource(env.workspace,'RASTER_WORKSPACE',arg[1])
					lyr.name = arg[1] 

##############################
# Change the picture element #
##############################
if change_picture == True:
	pic = ListLayoutElements(mxd, "PICTURE_ELEMENT", "thumbnail_map")[0]
	for arg in args[4:]:
		arg = arg.split('=')
		if arg[0] == 'thumbnail_picture':
			pic.sourceImage = arg[1]

#################
# Change extent #
#################
if change_extent == True:
	df	 = 	ListDataFrames(mxd)[0]
	newExtent = df.extent
	for arg in args[4:]:
		arg = arg.split('=')
		if arg[0] == 'extent_map':
			addLayer = Layer(arg[1])
			AddLayer(df, addLayer, "BOTTOM") # add new layer at the bottom of the dataframe
			lyr = ListLayers(mxd)[-1]
			newExtent = lyr.getExtent() # update extent from new source layer
		if arg[0] == 'xmin':
			newExtent.XMin = float(arg[1])
		if arg[0] == 'xmax':
			newExtent.XMax = float(arg[1])
		if arg[0] == 'ymin':
			newExtent.YMin = float(arg[1])
		if arg[0] == 'ymax':
			newExtent.YMax = float(arg[1])

	# Save 
	mxd.save()		
	df.extent = newExtent
	del newExtent

# Save 
mxd.save()

# Delete
del mxd
