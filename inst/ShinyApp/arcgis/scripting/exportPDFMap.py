# Name: exportMap.py
# Description: Export one map document as PDF 
# Requirements: ArcGIS Mapping Extension

# Import system modules
import re
import arcpy
from arcpy import env
from arcpy.mapping import *

# Get command line arguments
import sys
args = sys.argv

# Define current working space 
env.workspace = args[1]

# Allow overwriting
env.overwriteOutput = True

# Set local variables
template	= args[2]
outdir		= args[3]

# Check out ArcGIS Mapping extension license
arcpy.CheckOutExtension('Mapping')

# open PDF document
PDFdoc = PDFDocumentCreate(outdir)

# read the original template 
mxd = MapDocument(template)

# output map
outmap = os.path.join(os.path.dirname(template),re.sub('_MapTemplate.mxd','.pdf',os.path.basename(template))).replace("\\","/")

# export
ExportToPDF(mxd,outmap,resolution=600)

# delete 
#del mxd

# save and close PDF document
PDFdoc.saveAndClose()

