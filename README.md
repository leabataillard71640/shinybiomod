[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0)
# **ShinyBIOMOD**
<img src="inst/ShinyApp/www/ShinyBiomodTeam.png" alt="alt text" width="75%" height="75%">

<br><br>
Welcome to ShinyBIOMOD , a platform for Ensemble Modelling of Species Distributions , that helps using a range of statistical tools to examine the relationship between species and their environment, project their distribution in space and time, assess prediction uncertainty.

- **Winner of the 2020 GBIF Ebbe Nielsen Challenge** ([see article](https://www.gbif.org/news/AcT155L4KYZ5RxsfDnGGt/shinybiomod-wins-2020-gbif-ebbe-nielsen-challenge))

- **Screencast for the Ecological Niche Modeling 2020 course** ([ENM2020](https://www.youtube.com/watch?v=vj8qTo56rPA "ENM2020"),[ShinyBIOMOD](https://www.youtube.com/watch?v=nhWPYeLhxoA&t=5s "ShinyBIOMOD"))**:**
[![alt text](https://img.youtube.com/vi/nhWPYeLhxoA/3.jpg)](https://www.youtube.com/watch?v=nhWPYeLhxoA&feature=youtu.be "alt text")

- **MDPI Diversity Journal Student Poster Prize winner** at the [Species On the Move 2019](http://www.speciesonthemove.com/) International Conference Series ([see poster](SOTM2019_poster.png)) 

# ***What is ShinyBIOMOD ?***

ShinyBIOMOD is an R application providing a user-friendly interface and new functionalities to **BIOMOD (BIOdiversity MODelling,** [biomod2](https://cran.r-project.org/web/packages/biomod2/biomod2.pdf) package), through the [shiny](https://cran.r-project.org/web/packages/shiny/shiny.pdf) package. 
It aims to facilitate the creation of Species Distribution Models (SDMs) by guiding the user step-by-step through the modelling process. The objective is to provide an easy tool for users with or without prior programming knowledge and improve visualization of data, while ensuring the user to get familiar with good practices in Species Distribution Modelling. 
It combines:

- <mark style="background-color: lightblue"> the ability of biomod2 to model species distributions with an ensemble of statistical tools</mark> and project them into different scenarios of past, present and/or future environmental conditions (e.g. climate, land cover...etc)

- <mark style="background-color: lightblue"> an easy-to-use interface implemented with shiny</mark> which allows to explore, test and visualize more easily a wide range of modeling approaches and spatial data.


ShinyBIOMOD is based on the R package biomod2, the last version of BIOMOD which uses a larger set of statistical models (including the popular MaxENT), evaluation metrics and more possibilities for manipulating background/pseudo-absence data. 
In addition to the functions from biomod2, ShinyBIOMOD also comprises new tools to ***(i) account for sampling biases*** in observation data, ***(ii) select environmental predictors*** mostly by minimizing (multi-)collinearity , and ***(iii) visualize inputs/outputs*** and ***export*** results.

Learn more about BIOMOD:
- [Concept, structure and philosophy](https://www.youtube.com/watch?v=-IAdf8Vh6uY&feature=youtu.be "Concept, structure and philosophy") 
- see also [Single species modelling](https://www.youtube.com/watch?v=QrwqhJgRbnY&feature=youtu.be "Single species modelling"), [Multi species modelling](https://www.youtube.com/watch?v=E2I5PSdwrG8&feature=youtu.be "Multi species modelling") and [Specifics](https://www.youtube.com/watch?v=hEjhdURRy3o&feature=youtu.be "Specifics")

## *Installation*
Beforehand, make sure to have [*R*](https://cloud.r-project.org/ "R") or [*Rstudio*](https://rstudio.com/products/rstudio/download/ "Rstudio") installed on your machine.

Some R packages need to be compiled from source, so if you are on Windows, you need to install [*Rtools*](http://cran.r-project.org/bin/windows/Rtools/) too.<br>

Install *ShinyBIOMOD* and run the application with the following instructions.<br>
If the package `devtools` is not already installed run `install.packages("devtools")` in your console.<br>
Setting <code>R_REMOTES_NO_ERRORS_FROM_WARNINGS="false"</code> will cause warning messages during calls to `devtools::install_gitlab` to become errors. So beforehand, make sure to set this environmental variable to `true`.
```R
Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS="true")
```
For some reasons, the installation will fail for the latest versions of *R* (>= 4.0) (and *shiny*) if the CRAN mirror repository is not set from within the user *.Rprofile* file. To successfully install the app:
- browse your folders to your *.Rprofile* (usually located in your R home directory */ etc / .Rprofile* otherwise, if you are using *Rstudio* and the package *usethis* is installed, the command *usethis::edit_r_profile()* will search your .Rprofile file and will open it (if it ever find it) in your *R* window to make edits)
- add the following lines to the file:
```R
local({r <- getOption("repos")
   r["CRAN"] <- "https://cloud.r-project.org"
   options(repos=r)
})
## You can replace *https://cloud.r-project.org* by any other CRAN mirror repository of your choice
```
- save and exit.

Then, start a new *R* session. *ShinyBIOMOD* uses a customized version of *biomod2*, so if you already have the 'native' *biomod2* package installed (i.e. not the *ShinyBIOMOD* version), you need to uninstall it first. You can type:
```R
utils::remove.packages("biomod2")
```
Then:

```R
devtools::install_gitlab("IanOndo/ShinyBIOMOD")
library(ShinyBIOMOD)
run_shinyBIOMOD()
```
If you ever meet issues with the installation of the package dependencies (e.g. biomod2 or maxent), I recommend dealing with them in the order in which issues appear. If you need to manually install biomod2, type `devtools::install_gitlab("IanOndo/shbiomod2")`.

# ***How does it work ?***

## *the workflow*
The scheme below represents the **general workflow** of ShinyBIOMOD. It describes how the user is guided throughout a series of mandatory and non-mandatory steps to build a **Species Distribution Model (SDM)** with the possibility of exploring and visualizing data at every of these steps. The workflow is designed to help the user to optimize the modelling process for best results and high efficiency. 
ShinyBIOMOD is an interactive tool that considers the data available to the user at each step before proceeding to the next ones. Indeed, new tabs and options appear dynamically according to the set of information entered by the user as he advances along the modelling process.

<img src="inst/ShinyApp/www/ShinyBiomod_workflow_chartv2.png" alt="alt text" width="75%" height="75%"> 

## *step-by-step*
## 0. Home page & working directory
Here is the homepage of *ShinyBIOMOD*, it essentialy welcomes the user with a brief about the app, the developement team and the research work r(references) the app is relying on.
By default the working directory is set to the "*HOME*" directory of the R(studio) session, if the user wants to change it, he just needs to navigate to the tab `Settings` and change the directory using the folder explorer in the `Set working directory` sub tab.

<br>

![](screenshots/screenshot_homepage_tab.png)

<br>

## 1. Data upload
Go to the `Data upload` tab and upload your occurrence and your environmental data. Occurrence data can be locally uploaded using a file explorer tool accessible in the sub tab `"Occurrence data/Upload your data"`. Environmental data can either be uploaded from:

-  A local disc, by using a file explorer tool in `Environmental data/User-defined`

-  WorldClim database, by downloading the data via an “api” located in the sub tab `Environmental data/WorldClim`.

### 1.1  User occurrence data
In the first screenshot below, the red frame highlights the left-hand side panel where you can import your occurrence data from. The black arrow indicates the file explorer widget. Once your file is uploaded, you will be prompted to select which columns in your dataset refer to the *longitude*, *latitude* and *species* name. Then, you will need to select which species you want to work on, and validate your choice.
Once your parameters are set, the  main panel will display an interactive map in the tab `Map` as illustrated in the screenshot. Tables and statistics are displayed in the tab `Tables` (the one towards which the upper black arrow points to).

<br>

![](screenshots/screenshot_user_occurrence_upload_tab2.png)

<br>

### 1.2  User-defined environmnental data
From this screenshot you can see the map of one environmental raster layer uploaded from the local disc. This page has a security check to make sure that the directory selected contains raster layers (a warning is displayed otherwise). The black arrow points towards a dropdown menu button allowing the user to:

-  select a number of layers to display,

-  pick up a color palette

Also, there is a note just under the folder explorer that recalls the user that all the rasters must have the same extent and the same resolution, otherwise the app will send an error message and probably crash.

<br>

![](screenshots/screenshot_user_raster_layers_tab2.png)

<br>

## 2. Data processing
You may have noticed that a new tab called `Data processing` popped up since you successfully uploaded and validated your data. 
This is for encouraging the user to pre-process his occurrence dataset before moving forward.<br>
One can:

-  spatially filter occurrence records in the `Spatial thinning/Thin your occurrence data`tab (if one suspects sampling issues).

-  constrain the geographic extent of the training area in the `Geographic constraints/Constrain your training area` tab, to account for limiting dispersal capacity of species for example.

<mark>The first screenshot is from the *Spatial thinning* tool, and describes the standard page layout displayed by most modules in *ShinyBIOMOD*:</mark>
- <mark>A brief description of features of the module (red frame)</mark>
- <mark>A set of parameters to fulfill before executing the routine (press button) (blue frame)</mark>
- <mark>The (map) output of the results followed by a short summary with certain information highlighted in colors to draw attention to these parameters. (green frame).</mark>


Both modules *Spatial thinning* and *Constraint of training area* allow the user to save his results for later use in the modelling process.

### 2.1 Spatial thinning
In the screenshot below, the thinning (filtering) parameter has been set to keep a minimum distance of 10 km between each records. The field "Number of replications" indicates that the filtering process for this particular example is replicated only once. The summary on the main panel indicates that the algorithm retained 120 records (out of > 500). Any records retained are mapped in green, and records removed in red.

<br>

![](screenshots/screenshot_thinning_tab2bis.png)

<br>

### 2.2 Constraint of the training area
In the example below, the geographical extent selected for model training is constrained (limited) by a 100 km *circular buffer* size around the records. 
The resulting background area is then clipped to the coastline (to avoid generating pseudo-absences in non terestrial lands).
Many other options to define the training area are available:

- Manual drawing of the training area (not implemented yet !)
- Using a pre-defined region (e.g. expert opinion map or the estimated dispersal range of your species)
- Using similarity in environmnental conditions (environmental profiling, not implemented yet)
- Using a bounding box or a convex hull

<br>

![](screenshots/screenshot_constrained_area_tab.png)

<br>

## 3. Data exploration
Similarly, you may have noticed that a new tab called `Data exploration` appeared since the uploading of environmental data. It encourages the user to explore the correlations among his environmental variables, and thus allow to prevent collinearity issues. 
When the user clicks on this tab,  he can find multiple sub tabs allowing to perform:

- PCA
- Sparse PCA
- Correlation analysis

### 3.1 Principal Component Analysis
The *Principal Component Analysis* (PCA) is particularly useful for detecting which variables best describe the variability in environmental conditions of a dataset. On the left-hand side, the user selects which transformation he wants to apply before applying the PCA and the number of dimensions in which he wants to reduce the environmental space (number of axis). Then, clicking on `Run PCA` will start the computations. The analysis produces 4 types of diagnostics:

- A barplot of the contribution of each axis (i.e. principal component) to the variability found in the dataset.
- A barplot of the contribution of each variable to each selected principal components.
- A correlation circle centred on the PCA projection plan.
- A raster of the (relative and/or absolute) contribution of each grid cell to the PCA projection plan (i.e. how well each grid cell is represented on this plan).

The app automatically displays the first 3 ones in the tabs `barplot` and `Correlation circle`. The fourth diagnostic must be selected from the menu options on the left-hand side panel. 

Variables ranked first in the first axis logically are the most relevant here.

<br>

![](screenshots/screenshot_PCA_barplot_tab.png)

<br>

On this screenshot, the horizontal axis is the first principal component, and the vertical one is the second principal component. 
Variables whose arrows are closer to a particular component, contribute more to this component. The length of an arrow represents the correlation of the variable to an axis (i.e. the longer, the greater the correlation is with a given axis). For example, *GLOBAL_SLOPE_10MIN* correlates more with the first principal component but its correlation is much lower compared to other variables.

<br>

![](screenshots/screenshot_PCA_corcircle_tab.png)

<br>

### 3.2 Correlation analysis
Moving onto the `Correlation analysis` tab, this screenshot shows the result of a correlation analysis between a set of variables. 50000 grid cells values from the rasters have been sampled, setting up a maximum pairwise correlation between variables of 0.5 (pretty gentle). The output displayed in the `Results` tab lists only two pairs of variables with correlations above this threshold. 
The highest correlation found is only -0.559, considering collinerity issues are usually flagged when correlations exceed 0.7, one can consider that there is no evidence of high collinearity among the variables. But there might still be multicollinearities (i.e. high correlations between more than 2 variables).
In that case, one can check other correlation methods:
- *VIF cor* 
- *VIF stepwise*

Both methods use the *Variance Inflation Factor* to exclude multicollinear variables (i.e. variables correlated to many others).
One can have a look at the correlation matrix (pairwise correlations among all variables) in the tab `Table(s)`

<br>

![](screenshots/screenshot_Correlations_tab.png)

<br>

### 3.3 Model selection
There is a fourth tab called `Best subsets` which remains hidden if occurrence dataset uploaded does not contain (true) absences. Indeed, this tab is disabled as long as the app does not detect a presence-absence dataset. Otherwise, one has the possibility to run a different model selection which consists in selecting a subset of environmental variables based on various criteria such as *information criteria* (AIC, BIC) and *cross-validation* (k-fold CV, leave-one-out).


## 4. Data preparation
Once the user is satisfied with his occurrence dataset and environmental layers, he can move to the `Data preparation` tab, select the data he wants to use from the left-hand side panel, and visualize them in the main panel on the right-hand side. Save the outputs and continue.

Selection fields on the left panel will propose to use *pre-processed* or *non pre-pocessed data* for model training, depending if the user saved his analyses as shown earlier (which is the case here). 
An interesting feature is that the app will detect if some correlation analyses (VIF cor or VIF step) have been perfomed, and will propose to look at the subset (if any) of variables recommended from those analyses.

On the screenshot below, you can see that both occurrence data and environmnental rasters can be displayed on the same interactive map. 
Optionnaly, one can also download a dataframe with environmental values extracted at occurrence locations. 

<br>

![](screenshots/screenshot_data_preparation_tab.png)

<br>

## 5. Ecological Niche Modelling
This is the core of the modelling process, where the user can:<br>
- define a *model-based* strategy to account for sampling bias in training records.
- select one or more model algorithm(s).
- choose the *functional form* of the response of species to environmental gradients...etc.

In this section, (`ENM`), each sub tab corresponds to one of the main features of the **biomod2** package: 
- Formatting
- Modelling
- Ensemble-Modelling

### 5.1 Formatting
In `formatting`, if the user does not have true absence locations (or if he has too) but only presence records, he can generate *pseudo-absences* (PA) locations in order to contrast ecological conditions at those sites, to the ones at presence sites.<br>
To do so, select:
- A number of PA datasets
- A number of PA locations
- A strategy for generating those PA locations i.e. how would you like them to be sampled randomly ? across your background, or according to a specific scheme ?

*ShinyBIOMOD* facilitates user-defined PA strategies by implementing 2 PA selection schemes used in the litterature to tackle sampling bias: 
- TargetGroup background selection scheme (TAG)
- Background thickening selection scheme (BT)

Both schemes sample PA locations from an estimate of sampling probablity based on the kernel density estimation of occurrence records. *TAG* uses occurrence records from a particular group of species including the user species (often from a higher taxonomic rank), and *BT* uses the training presences of the user species.

One needs to:<br>
1.  upload your occurrence records as before, making sure to respect the format required.<br>
2.  select a bandwidth for the kernel density estimation<br>
3.  generate a sampling probability surface<br>
4.  sample your PA locations<br>
5.  save your PA dataset(s)<br>

All those steps are included in the dropdown menu `Pseudo-absences selection` located on the left-hand panel as on can see on the screenshot below.
Only *user-defined* strategies are displayed on the main panel. Black dots represent the training presences, green dots the PA locations.

Once everything is settled, clicking on the button `Run Formatting` will reshape the data. Wait while the app is computing and check the results. 

As already mentioned before, important information are highlighted with colors.
<br>

![](screenshots/screenshot_biomod_formatting_tab.png)

<br>

If the formatting of the data is done with success, it triggers the next tab : `Modelling`.

### 5.2 Modelling
In `Modelling`, one can (non exhaustive list):
- choose between 10 model algorithms to run
- choose several model tuning options including e.g. the form(ula) of the relationship between species occurrence and environmental variables for regression-like models (in the red dropdown menu button under the selection field of model algorithm(s))
- choose a set of performance metrics to evaluate model(s)
- compute variable importance (i.e. how much the environmental suitability of your species changes when the variable is dropped out of the set of predictors)
- ...

A particularly interesting feature of *ShinyBIOMOD* is that one can save those parameters by clicking on a `save` button at the bottom end of the left-hand side panel. Once back to this section, the app will ask the user if he wants to load previously saved modelling parameters. Answering "YES" will automatically populate the fields with previous parameters.

Click on `Run BIOMOD Modeling` to fit the model(s). Check out the usual output.

<br>

![](screenshots/screenshot_biomod_modelling_tab.png)

<br>

### 5.3 Ensemble-Modelling
An `Ensemble-Modelling` tab appears and allow the user to choose which and how (which assembly rule) model(s) should be combined together.

Then, he must select a method to generate predictions from multiple models, i.e. how the predictions of each model should be combined ? 
- mean prediction ? 
- weighted mean ?
- ...

One can also choose to generate uncertainty values also such as the coefficient of variation of the predictions.

The ouput from this module is similar to the `Modelling` one so, it is okay to skip it and navigate to the `Evaluations` tab.

## 6. Evaluations
Here the user can check:

-  how well or bad the models have performed in predicting the presence of species <i class='fa fa-check'></i>
-  which variables seems to be important for predicting species occurrence
-  the plausibility of the response shape of the species to the predictors used to model its distribution

### 6.1 Performance metrics
In the `Performance metrics` tab, on the left-hand side panel, the user must select the type of models to check (individual models or ensemble-models ?), the performance metric, PA dataset...etc.

Clicking on the `check` button will display the results.
A barplot displays the variable importance score in the lower right corner of the main panel. Scores are ordered from the highest to the lowest for better visualization.
On the right, a data table shows evaluation metric scores selected, with additional information such as the threshold value associated with the confusion matrix used to calculate those metrics.
<br>

![](screenshots/screenshot_perf_metric_and_variable_importance_tab.png)

<br>

### 6.2 Response function
In the `Response function Uni/Bivariate` sub tab, one can inspect how a species respond to environmental predictors.
The response is scaled between 0 [low probability of presence] and 1 [high probability].
Same as for the `Performances metrics` sub tab, one can specify which model to check on the left-hand side panel.
Univariate response curves can be traced for each variable, while the others are held at a constant metric value to be selected among: 
the *mean* (default), the *median*, the *minimum* or the *maximum*.
<br>

![](screenshots/screenshot_response_curves_tab.png)

<br>

## 7. Projections
This is the last tab that will be triggered if the user have successfully run an Ensemble-Model. At the moment only the `Ensemble Forecasting` tab is enabled since no ensemble-model projections have been generated yet.

In this tab, one need to select (non exhaustive list):
- the geographic extent on which projecting the model(s)
- a set of environmental variables: <mark> make sure they have the same names as the ones used for model training. </mark>
These variables can be past or future projections of the ones used for training.
- which ensemble-model(s) to project.

Finally, clicking on `Run BIOMOD EnsembleForecasting` will run the ensemble-prediction. Check the output.
I will spare you the screenshot of this output since it is similar to the other modelling outputs.

Once one has generated the projections of ensemble-model(s), 3 new tabs appear:
- `Thresholding` (Binary maps)
- `project as a raster` and `project onto geography` (Project maps)

### 7.1 Thresholding
The `Thresholding` tab will help to convert probability map(s) generated by the ensemble-predictions into binary maps (i.e.presence/absence map).

Several criteria are available to do this conversion:
- manually selecting a threshold i.e. selecting a probability range within (inversely beyong) which one considers that the species is present (inversely absent) from an area
- selecting one or more criteria that will calculate optimized thresholds for this conversion.

Then, clicking on `Convert your map` will run the conversion. 

As shown in the screenshot below, the output produces as many maps as criteria selected (see black arrows), a tab summarizing the methods (criteria) used, the optimized threshold(s) computed, the entries on the confusion matrix computed and an estimation of the occupied area predicted by this range map. The green circle on the lower right corner shows a legend to better understand the confusion matrix entries.

As one can see, different methods/criteria lead to different predicted distribution area, thus different range size estimations.
<br>

![](screenshots/screenshot_binary_conversion_tab.png)

<br>

There are options to save those binary maps, for visualization with the interactive tool in the `Project onto geography` tab for example.

### 7.2 Projection maps
Finally, in the following tabs one can find the map of the predictions displayed by a raster plot (first screenshot), and maybe more interestingly, displayed on an interactive map (second screenshot), since one can zoom in/out, drag the map to some particular areas, and add occurrence records on top of predictions.
<br>

![](screenshots/screenshot_project_raster_tab.png)

<br>

<br>

![](screenshots/screenshot_project_leaflet_raster.png)

<br>


## *the folder structure*
The workflow described previously automatically generates a project with folders and files associated to the user activity during his session. 
The project is a single root directory named `SHBMD` (abbr. of ShinyBIOMOD) and is hierarchically structured, starting with a limited number of general folders, then broadens towards more specific folders including those generated by the `biomod2` package.

By saving the user workflow in a self-contained organised folder system struture, *ShinyBIOMOD* ensures **reproducibility** of your research, **provenance** of your inputs and **portability** of your results (e.g. when moving from computer to computer).
A detailled scheme of the folder structure will be available soon from the home page of the app.

Here is a quick example of how the app responds when the user enters a (working) directory containing a *ShinyBIOMOD* project.
It simply informs the user that the output of a model previously run is available for display (example taken from the `Modelling...` sub tab).

<br>

![](screenshots/screenshot_click_here.png)

<br>



## ***Known issues***

### *reactivity*
*Reactivity* is what makes the app responsive to the user inputs. It updates the app whenever the user makes changes. In *ShinyBIOMOD* some expressions are very complex, with a deep graph dependency, so re-running your analysis in a very short amount of time may cause the app to become unresponsive, bogging down or even crash. I'm working on improving the reactivity of certain sections to avoid that and enhance users' experience. 
Meanwhile, if for some reasons the app does not respond as expected, e.g. tabs that do not show up, outputs that do not display...etc, please try the following options:<br>
1. ...Wait...<br>
2. Re-run the analysis (re-click the button)<br>
3. Navigate to another tab, then come back to your current one<br>
4. Close and re-start the app (model objects generated are automatically saved and will be available when accessing your working directory)<br>
5. Check out the log files generated by the biomod2 modelling sections to see if any error has been reported. You can find this file here:<br>
`<your working directory> --> SHBMD --> SavedObj --> MyBioModData --> (Formating|Modeling) --> <your species name> --> <species.name>_<biomod_procedure>Out.log`

### *multispecies*
Multispecies modelling is still experimental and works fine for simple workflows. However, when modelling multiple species with a different set of parameters, (hundreds of) R expressions are evaluated for each species, which creates a very complex reactivity structure.
A recommendation to avoid problems when modelling multiple species:
- Run your analyses one species at a time *(this is the default in the modelling section)*
<br>
<mark>*ShinyBIOMOD* is not designed to model say, more than half a dozen species at the time</mark>.Visualization tools and computations may not respond well. The app works best with one or two species.

# ***References***

Thuiller, W., Lafourcade, B., Engler, R. and Araújo, M. B. 2009. BIOMOD - a platform for ensemble forecasting of species distributions. - Ecography 32: 369 - 373. [DOI:10.1111/j.1600-0587.2008.05742.x](https://doi.org/10.1111/j.1600-0587.2008.05742.x)

Thuiller, W., Georges, D., Engler, R. & Breiner, F. Package "biomod2"; (2016)
